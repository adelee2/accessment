<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home</title>
    <link type="text/css" href="assets/css/css/bootstrap.css" rel="stylesheet"/>
    <link type="text/css" href="assets/css/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

<style>
    a{
        padding-right:20px;
    }
</style>
</head>

<body>
    <div class="container">
        <form style="width:50%;margin:10% 30%" class="form-inline">
            <div class="mb-3 mx-auto" style="text-align:center">
                <span><a href="socialauth.php?provider=Google" class="m-6"><img src="assets/imgs/google-logo.jpg" width="30"/></a><a href="socialauth.php?provider=Facebook"><img src="assets/imgs/facebook-logo.png" width="30"/></a><a href="socialauth.php?provider=Twitter"><img src="assets/imgs/Twitter-Logo.png" width="30"/></a></span>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
   <!-- jQuery -->
   <script src="assets/vendor/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="assets/vendor/popper.min.js"></script>
<script src="assets/vendor/bootstrap.min.js"></script>


</body>

</html>