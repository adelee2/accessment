<?php
    include('vendor/autoload.php');
    $loader = new josegonzalez\Dotenv\Loader('.env');
    $keys = $loader->parse()->toArray();
   
    if(isset($_GET['provider'])){
        $provider = $_GET['provider'];
        if($provider == "Facebook"){
            $config = [
                // Location where to redirect users once they authenticate with Facebook
                // For this example we choose to come back to this same script
                'callback' => 'http://localhost:8888/SocialAuth/question1/dashboard.php',
            
                // Facebook application credentials
                'keys' => [
                    'id' => $keys['FACEBOOK_ID'], // Required: your Facebook application id
                    'secret' => $keys['FACEBOOK_SECRET'] // Required: your Facebook application secret 
                ]
            ];
            
            try {
                // Instantiate Facebook's adapter directly
                $adapter = new Hybridauth\Provider\Facebook($config);
            
                // Attempt to authenticate the user with Facebook
                $adapter->authenticate();
            
                // Returns a boolean of whether the user is connected with Facebook
                $isConnected = $adapter->isConnected();
                if($isConnected){
                    // Retrieve the user's profile
                    $userProfile = $adapter->getUserProfile();
                    // Inspect profile's public attributes
                    var_dump($userProfile);
                    // Disconnect the adapter (log out)
                    $adapter->disconnect();
                }
                else{
                    echo "cannot connect";
                }
                
            }
            catch(\Exception $e){
                echo 'Oops, we ran into an issue! ' . $e->getMessage();
            }
        }
        elseif($provider == "Google"){
            $config = [
                // Location where to redirect users once they authenticate with Google
                // For this example we choose to come back to this same script
                'callback' => 'http://localhost:8888/SocialAuth/question1/dashboard.php',
            
                // Google application credentials
                'keys' => [
                    'id' => $keys['GOOGLE_ID'], // Required: your Google application id
                    'secret' => $keys['GOOGLE_SECRET']  // Required: your Google application secret 
                ]
            ];
            
            try {
                // Instantiate Google's adapter directly
                $adapter = new Hybridauth\Provider\Google($config);
            
                // Attempt to authenticate the user with Google
                $adapter->authenticate();
            
                // Returns a boolean of whether the user is connected with Google
                $isConnected = $adapter->isConnected();
             
                // Retrieve the user's profile
                $userProfile = $adapter->getUserProfile();
            
                // Inspect profile's public attributes
                var_dump($userProfile);
            
                // Disconnect the adapter (log out)
                $adapter->disconnect();
            }
            catch(\Exception $e){
                echo 'Oops, we ran into an issue! ' . $e->getMessage();
            }
        }
        elseif($provider == "Twitter"){
            $config = [
                // Location where to redirect users once they authenticate with Twitter
                // For this example we choose to come back to this same script
                'callback' => 'http://localhost:8888/SocialAuth/question1/dashboard.php',
            
                // Facebook application credentials
                'keys' => [
                    'id' => '...', // Required: your Twitter application id
                    'secret' => '...'  // Required: your Twitter application secret 
                ]
            ];
            
            try {
                // Instantiate Twitter's adapter directly
                $adapter = new Hybridauth\Provider\Twitter($config);
            
                // Attempt to authenticate the user with Twitter
                $adapter->authenticate();
            
                // Returns a boolean of whether the user is connected with Twitter
                $isConnected = $adapter->isConnected();
             
                // Retrieve the user's profile
                $userProfile = $adapter->getUserProfile();
                // $_SESSION['userprofile'] = $userProfile
                
                // Inspect profile's public attributes
                var_dump($userProfile);
            
                // Disconnect the adapter (log out)
                $adapter->disconnect();
            }
            catch(\Exception $e){
                echo 'Oops, we ran into an issue! ' . $e->getMessage();
            }
        }
        else{
            echo "<script> alert('Unknown Provider')</script>";
        }
    }
    else{
        header("Location: index.php?error=invalid_login");
    }
?>